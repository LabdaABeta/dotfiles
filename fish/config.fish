fish_vi_key_bindings

function fish_prompt
    set EXIT $status
    set_color normal --bold
    echo -n '[ '
    set_color blue
    echo -n (prompt pwd)
    set_color normal --bold
    echo ' ]'
    set_color green

    if [ $EXIT != 0 ]
        set_color --background white black
    else
        set_color green
    end

    echo -n $USER
    set_color normal
    echo -n '@'
    set_color magenta
    echo -n (hostname -s)
    set_color normal
    echo ' $ '
end
function th
    ~/.config/themeswitch.sh $argv
end

function themegen
    themer render all -t myi3
end

function termcolours
    ~/.config/colorscheme.sh
end

function :q
    exit
end